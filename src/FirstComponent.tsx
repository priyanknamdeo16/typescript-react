import * as React from "react";
import { IUser } from "./interfaces/Iuser";

// container.tsx - now you can import your component in both ways, named export (good encapsulation) or using default export
import Asset from "./guides/asset";

import SFCComponent from "./guides/sfc/component";
import { StatefulAge } from "./guides/stateful/stateful";
import TestNode from "./guides/sfc/nodes";
import StatefulAgeWithDefault from "./guides/stateful_defaultprops/statefulDefault";
import generic from "./guides/generic/generic";
import { NameProvider } from "./guides/renderProps/props";
import { Mouse } from "./guides/renderProps/renderFunction";
// import HocUsage from "./guides/hoc/hocUsage";


export default class FirstComponent extends React.Component<IUser, any> {
  constructor(props: IUser) {
    super(props);
    this.state = { lazyComponent: null, initialAge : 20 };
  }

  async loadLazyComponent() {
    var component = await import(/* webpackChunkName: "Second1"*/ "./SecondComponent");
    var ele = React.createElement(component.default);
    this.setState({ lazyComponent: ele });
  }
  onIncrement= () => {

  };
  handleClick = (e:any) => {
    console.log(e);
    this.loadLazyComponent();
    // import("./SecondComponent").then(widget => {
    //   var ele = React.createElement(widget.default);
    //   this.setState({ lazyComponent: ele });
    // });
  };
  handleClickAge = () => {
    
    this.setState({ initialAge: 50 });
  
  };
  render() {
    return (
      <div id="one">
        <div><TestNode name="aa"/></div>
        <br/>
        <div>Generic list</div>
        {generic()}
        <br/>
        <div>Stateful component</div>
        <StatefulAge name="priyank"/>
        <br/>
        <div>Stateful component with default props</div>
        <StatefulAgeWithDefault name="Priyank with default prop" initialAge={this.state.initialAge}/>
        <button onClick={this.handleClickAge}>
          Check - Initial Age
        </button>
        <br/>
        <div>SFC component</div>
        <SFCComponent label="one" count={10} onIncrement={() => this.onIncrement()}></SFCComponent>
        <br/>
        <br/>
        <div>Render props</div>
        <NameProvider>
          {({name}) => (
              <div>Coming from render props {name}</div>
            )
          }
        </NameProvider>
        <div>Render props with render function</div>
        <Mouse render={({x, y}) => (
              <div>Coming from render props {x}, {y}</div>
            )
          }>
        </Mouse>
        {/* <HocUsage isLoading={false}></HocUsage> */}
        <h1>{this.props.name}</h1>
        <button onClick={e => this.handleClick(e)}>
          Load Second component111...
        </button>
        <Asset value="111" />
        {this.state.lazyComponent}
      </div>
    );
  }
}
