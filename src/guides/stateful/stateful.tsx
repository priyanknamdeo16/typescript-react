import * as React from 'react';

export interface IProps {
  readonly name: string;
}

interface IState {
  readonly age: number;
}

export class StatefulAge extends React.Component<IProps, IState> {
  // state initialization  
  readonly state: IState = {
    age: 0,
  };

  incrementAge = () => {
    this.setState({ age: this.state.age + 1 });
  }

  render() {
    const { incrementAge } = this;
    const { name } = this.props;
    const { age } = this.state;
    return (
      <div>
        <span>Hello {name}, your age is {age} </span>
        <button type="button" onClick={incrementAge}></button>
      </div>
    );
  }
}
