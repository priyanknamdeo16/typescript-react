export interface IProps {
    label: string;
    count: number;
    onIncrement: () => any;
  }