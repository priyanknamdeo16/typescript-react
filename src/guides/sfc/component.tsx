import * as React from 'react';

// import interface
import { IProps } from "./iProp";

const SFCComponent: React.SFC <IProps> = (props) => {
  const { label } = props;
  return (
    <div>
      <span>{label}</span>
    </div>
  );
};

export default SFCComponent;