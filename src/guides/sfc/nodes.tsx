import { IProps } from '../stateful/stateful';

import * as React from 'react';

const TestNode: React.SFC <IProps> = (props) => {
    const { name } = props;
    const styles: React.CSSProperties = { color: 'red' } 
    return (
      <div style={styles}>
        <span>{name}</span>
      </div>
    );
};

export default TestNode;