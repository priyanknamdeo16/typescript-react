import * as React from 'react';

export interface IProps {
  name: string;
  initialAge?: number;
}
interface IState {
  readonly age: number;
}
interface DefaultProps {
  readonly initialAge: number;
}

const StatefulAgeWithDefault: React.ComponentClass<IProps> =
  class extends React.Component<IProps & DefaultProps> {
    // to make defaultProps strictly typed we need to explicitly declare their type
    // @see https://github.com/DefinitelyTyped/DefinitelyTyped/issues/11640
    static defaultProps: DefaultProps = {
        initialAge: 10,
    };
    
    readonly state: IState = {
      age: this.props.initialAge,
    };
    
    static getDerivedStateFromProps(nextProps, prevState) {
      console.log(nextProps, prevState);
      if(!prevState.age || prevState.age !== nextProps.initialAge) {
        return { age: nextProps.initialAge} ;
      }
      return null;
    }

    render() {
      const { name } = this.props;
      const { age } = this.state;
      return (
        <div>
          <span>{name}: {age} </span>
        </div>
      );
    }
  };

  export default StatefulAgeWithDefault;