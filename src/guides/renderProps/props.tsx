import * as React from 'react';

interface INameProps {
  // function representation  
  children: (state: INameState) => React.ReactNode;
}

interface INameState {
  // should be readonly
  readonly name: string;
}

export class NameProvider extends React.Component<INameProps, INameState> {
  // state should be read only  
  readonly state: INameState = { name: 'AM' };

  render() {
    return this.props.children(this.state);
  }
}