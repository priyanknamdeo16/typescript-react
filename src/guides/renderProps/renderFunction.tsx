import * as React from 'react';

export interface IProps {
  render: (state: IState) => React.ReactNode;
}

// readonly fields
interface IState {  
  readonly x: number;
  readonly y: number;
}

export class Mouse extends React.Component<IProps, IState> {
  // state initialization  
  readonly state: IState = { x: 0, y: 0 };

  // handler, arrow function
  handleMouseMove = (event: React.MouseEvent<HTMLDivElement>) => {
    this.setState({
      x: event.clientX,
      y: event.clientY,
    });
  };

  handleClick = (event: React.MouseEvent<HTMLElement>) => {
    console.log(event);
  };

  render() {
    return (
      <div onMouseMove={this.handleMouseMove} onClick={this.handleClick}>
        {this.props.render(this.state)}
      </div>
    );
  }
}