// import * as React from 'react';

// export interface WithLoadingProps {
//     isLoading: boolean;
// }

// export const withLoading = <P extends object>(WrappedComponent: React.ComponentType<P>) => 
//     class WithLoading extends React.Component<P & WithLoadingProps> {
//         static readonly WrappedComponent = WrappedComponent;

//         componentDidMount() {
//             // isLoading to be set either in prop or state
//         }
//         render() {
//             const { isLoading, ...props } = this.props as WithLoadingProps;
//             return (
//                 isLoading ? <div>Loading...</div> : <WrappedComponent {...props}/>
//             );
//         }
//     };


