import * as React from 'react';

// ****************************
// Generic component definition
// ****************************
interface GenericListProps<T> {
  items: T[];
  itemRenderer: (item: T) => JSX.Element;
}

// generic list
class GenericList<T> extends React.Component<GenericListProps<T>, {}> {
  render() {
    const { items, itemRenderer } = this.props;
    return (
      <div>
        {items.map(itemRenderer)}
      </div>
    );
  }
}

// *************************
// Extension of generic
// *************************
interface IUser {
    name: string; surname: string;
}
class User implements IUser {
    constructor(public name, public surname) {}
}

// item data
const users = [
  new User('Priyank', 'Namdeo'),
  new User('Narendra', 'Modi'),
];

// user list
export class UserList extends GenericList<IUser> { }

export default () => (
  <UserList
    items={users}
    itemRenderer={(item) => <div key={item.name}>{item.name} {item.surname}</div>}
  />
);