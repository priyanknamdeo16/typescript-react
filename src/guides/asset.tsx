import * as React from "react";

// should be in another file
interface IProp {
  value: string;
}

// SFC component
const Asset: React.SFC<IProp> = props => {
  return <div>{props.value}</div>;
};

export default Asset;
