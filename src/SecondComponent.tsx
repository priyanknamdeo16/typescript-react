import * as React from "react";

// let Logo = "https://logrocket.com/img/logo.png";

export default class SecondComponent extends React.Component<{}> {
  render() {
    console.log("Welcome");
    return (
      <div id="one">
        {process.env.NODE_ENV}
        {/* React components must have a wrapper node/element */}
        <h1>Second component1</h1>
      </div>
    );
  }
}
