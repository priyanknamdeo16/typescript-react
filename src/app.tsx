import * as React from "react";
import * as ReactDOM from "react-dom";
import * as _ from "lodash";

import FirstComponent from "./FirstComponent";
// import SecondComponent from "./SecondComponent";

import "./style.css";

//only cube is accessed, other method is subject to tree shaking during production build
import { cube } from "./math.js";

// @ts-ignore
import Icon from "./photo.jpg";

ReactDOM.render(
  <div>
    <h1 className="value">
      This is from parent component, using a method cube of 5 is {cube(5)}
      yyy oooo99999++++++
    </h1>
    <img src={Icon} />
    <FirstComponent name="priyank namdeo" age="two" />
    {/* <SecondComponent /> */}
  </div>,
  document.getElementById("root")
);

if (module.hot) {
  module.hot.accept();

  // module.hot.accept("./print.js", function() {
  //   console.log("Accepting the updates..........");
  // });
}

ReactDOM.hydrate;
