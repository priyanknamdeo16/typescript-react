var path = require("path");
var HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
var ManifestPlugin = require("webpack-manifest-plugin");
const webpack = require("webpack");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;

var config = {
  entry: {
    index: ["./src/app.tsx", "webpack-hot-middleware/client"]
    //print: ["./src/print.js", "webpack-hot-middleware/client"]
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "awesome-typescript-loader",
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif|ttf)$/,
        use: ["file-loader"]
      }
    ]
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".png", ".jpeg"]
  },
  output: {
    filename: "[name].[hash].bundle.js",
    path: path.resolve(__dirname, "dist"),
    publicPath: "/"
  },
  optimization: {
    splitChunks: {
      chunks: "all"
    }
  },
  plugins: [
    //manifest
    new ManifestPlugin(),
    new CleanWebpackPlugin(["dist"]), //cleaning
    new HtmlWebpackPlugin({
      template: "./src/index.html"
    }),
    new webpack.HotModuleReplacementPlugin() //HMR plugin, typescript has issues
    // new BundleAnalyzerPlugin({ analyzerMode: "static" })
  ]
};

module.exports = config;
