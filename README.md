- Webpack features 
- Dynamic imports - works 
- Source maps - devtools - works 
- webpack dev server
- webpack-middleware
- Tree shaking - works 
Tree shakings steps - only production code get tree shaked

Use ES2015 module syntax (i.e. import and export).
Add a "sideEffects" property to your project's package.json file. false indicates no side effects
Use production mode configuration option to enable various optimizations including minification and tree shaking.

A "side effect" is defined as code that performs a special behavior when imported, other than exposing one or more exports. An example of this are polyfills, which affect the global scope and usually do not provide an export.

- Code splitting - works 
- Lazy loadin - works 
- 

Hot module replacement - not works for typescript, it needs babel transpiler
- https://github.com/gaearon/react-hot-loader/tree/master/examples/typescript

Error: module.hot is not found 

Solution: yarn add @types/webpack-env -D

HMR with typescript - example

/Users/pnamdeo/Documents/react/react-hot-loader/examples/typescript

http://gaearon.github.io/react-hot-loader/

- Does not work well with typescript

 Compile TS with TS compiler and then transpile down to ES5 with babel